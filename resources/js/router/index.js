import { createRouter,createWebHistory } from "vue-router";
import IndexAdmin from '../components/admin/Index.vue';
import HomePageIndex from '../components/pages/Index.vue';
import NotFound from '../components/NotFound.vue';
import Login from '../components/auth/Login.vue';

const routes = [
    {
        path: '/admin',
        name: 'dashboard',
        component: IndexAdmin,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/',
        name: 'home',
        component: HomePageIndex,
    },

    //login 
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            requiresAuth: false
        }
    },


    {
        path: '/:pathMatch(.*)*',
        name: 'notFound',
        component: NotFound,
        meta: {
            requiresAuth: false
        }
    }
];
const router = createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to,from) => {
    if(to.meta.requiresAuth && !localStorage.getItem('token')){
        return { name: 'login'}
    }

    if(to.meta.requiresAuth == false && localStorage.getItem('token')){
        return { name: 'dashboard'}
    }
})
export default router;